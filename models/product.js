const mongoose = require('mongoose');

var productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre: String,
    imagen: String,
    descripcion: String
})

module.exports = mongoose.model('Product', productSchema, 'productos');