const mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nombre: String,
    apellido: String,
    username: String,
    password: String,
})

module.exports = mongoose.model('User', userSchema, 'usuarios');