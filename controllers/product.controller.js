const express = require('express');
const router = express.Router();
const productService = require('../services/product.service');
const Product = require('../models/product');
const mongoose = require('mongoose');

router.get('/', getAllProducts);
router.post('/', createProduct);
router.delete('/', deleteProduct);

function getAllProducts(req, res, next){
    productService.getProducts()
        .then((p)=>{res.json(p)})
        .catch((err)=>next(err))
}

function createProduct(req, res, next){

    var newProduct = new Product( {
        _id: new mongoose.Types.ObjectId(),
        nombre: req.body.nombre,
        imagen: req.body.imagen, 
        descripcion: req.body.descripcion
    });

    productService.createProduct(newProduct, res, next)
        .then((prod)=>{
            console.log('prod',prod)
            res.json(prod)
        })
        .catch((err)=>next(err))
}

function deleteProduct(req, res, next){
    console.log(req.body);
    productService.deleteProductById(req.body.id)
        .then((prod)=>{res.json(prod)})
        .catch((err)=>next(err))
}

module.exports = router;