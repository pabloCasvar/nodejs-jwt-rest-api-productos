﻿const express = require('express');
const router = express.Router();
const userService = require('../services/user.service');
const config = require('../config.json');
const jwt = require('jsonwebtoken');

router.post('/authenticate', authenticate);

module.exports = router;

function authenticate(req, res, next) {
    userService.getUsers()
        .then(users => {
            const user = users.filter(u => u.username === req.body.username && u.password === req.body.password)
            if (user.length) {
                const token = jwt.sign({ sub: user._id }, config.secret);
                res.json(
                    {
                        nombre: user[0].nombre,
                        apellido: user[0].apellido,
                        token: token
                    }
                ) 
            } else {
                res.status(400).json({ message: 'Username or password is incorrect' })
            }
        })
        .catch(err => next(err));
}