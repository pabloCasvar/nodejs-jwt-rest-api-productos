﻿require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('./services/jwt');
const errorHandler = require('./services/error-handler');
const mongoose = require('mongoose');
const config = require('config.json');

const port = process.env.PORT || 4000;
const connection =  config.db || 'mongodb://localhost/apidb'


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.use(jwt());

app.use('/product', require('./controllers/product.controller'));
app.use('/user',     require('./controllers/user.controller'))

app.use(errorHandler);

console.log(connection)

mongoose.connect(connection)
    .then(()=>{
        const server = app.listen(port, function () {
            console.log('Server port:' + port);
        });        
    })
    .catch(e=> {
        return console.log(e)
    })