const Product = require('../models/product');
const mongoose = require('mongoose')

async function getProducts(){
    return Product.find({})
}

async function createProduct(newProduct){
    return newProduct.save();
}

async function deleteProductById(id){
    console.log(id)
    return Product.findByIdAndRemove(id);
}

module.exports = {
    getProducts,
    createProduct,
    deleteProductById,
}