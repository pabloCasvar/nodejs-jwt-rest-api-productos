const User = require('../models/user');
const mongoose = require('mongoose')

async function getUsers(){
    return User.find({})
}

async function findUser(u){
    return User.find(u)
}

module.exports = {
    getUsers,
    findUser,
}